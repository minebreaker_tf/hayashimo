package rip.deadocde.hayashimo

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import rip.deadcode.hayashimo.Configuration
import rip.deadcode.hayashimo.SqlRunner
import rip.deadcode.hayashimo.formatter.CsvFormatter
import rip.deadcode.hayashimo.formatter.Formatter
import java.io.StringWriter
import java.sql.Connection
import java.sql.DriverManager

internal class SqlRunnerTest {

    var connection: Connection = createConnection()
    var config = mock(Configuration::class.java)
    var runner = SqlRunner(connection, CsvFormatter(config))
    var writer = StringWriter()
    private fun createConnection(): Connection = DriverManager.getConnection("jdbc:h2:mem:")
    private fun createRunner(connection: Connection, formatter: Formatter) = SqlRunner(connection, formatter)

    @BeforeEach
    fun setUp() {
        connection = createConnection()
        config = mock(Configuration::class.java)
        `when`(config.header).thenReturn(false)
        runner = createRunner(connection, CsvFormatter(config))
    }

    @Test
    fun test() {
        runner.run("create table user (id int primary key auto_increment, name varchar not null)", writer)
        writer.write("\n")
        runner.run("insert into user (name) values ('John')", writer)
        writer.write("\n")
        runner.run("insert into user (name) values ('Jack')", writer)
        writer.write("\n")
        runner.run("select id, name from user", writer)
        assertThat(writer.toString()).isEqualTo("""0
1
1
1,John
2,Jack
""")
    }
}
