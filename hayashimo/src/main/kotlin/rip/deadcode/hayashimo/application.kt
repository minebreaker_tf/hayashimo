package rip.deadcode.hayashimo

import com.google.common.io.CharStreams
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.HelpFormatter
import org.eclipse.jetty.server.Request
import org.eclipse.jetty.server.handler.AbstractHandler
import rip.deadcode.hayashimo.formatter.CsvFormatter
import rip.deadcode.hayashimo.formatter.Formatter
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.io.StringWriter
import java.nio.charset.StandardCharsets
import java.sql.DriverManager
import java.sql.SQLException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

fun main(args: Array<String>) {

    val parser = DefaultParser()
    val commandLine = parser.parse(options, args)

    when {
        commandLine.hasOption("help") -> {
            HelpFormatter().printHelp("hayashimo", options)
        }
        commandLine.hasOption("server") -> {
            val config = readConfig(commandLine.getOptionValue("config"), commandLine)
            Server.run(config)
        }
        commandLine.argList.isEmpty() -> {
            val config = readConfig(commandLine.getOptionValue("config"), commandLine)
            Interactive.run(config)
        }
        else -> {
            val config = readConfig(commandLine.getOptionValue("config"), commandLine)
            val runner = SqlRunner(DriverManager.getConnection(config.url, config.user, config.password), CsvFormatter(config))
            runner.run(commandLine.argList[0], OutputStreamWriter(System.out))
        }
    }
}

object Interactive {

    private val reader = BufferedReader(InputStreamReader(System.`in`))

    fun run(config: Configuration) {

        val connection = DriverManager.getConnection(config.url, config.user, config.password)  // TODO properties
        val formatter = Formatter.getFormatter(config.format, config)
        val runner = SqlRunner(connection, formatter)

        while (true) {
            print("> ")
            // TODO 入力補完とか
            val sql = reader.readLine()
            val writer = StringWriter()
            try {
                runner.run(sql, writer)
                println(writer.toString())
            } catch (e: Exception) {
                formatter.formatError(e, writer)
            }
        }
    }
}

object Server {

    fun run(config: Configuration) {

        val connection = DriverManager.getConnection(config.url, config.user, config.password)  // TODO properties

        val server = org.eclipse.jetty.server.Server(config.port)
        server.handler = object : AbstractHandler() {
            override fun handle(target: String, baseRequest: Request, request: HttpServletRequest, response: HttpServletResponse) {

                if (baseRequest.method != "POST") {
                    baseRequest.isHandled = true
                    response.status = 405
                    return
                }

                val (sql, formatter) = when {
                    request.getHeader("accept").contains("application/json") -> {
                        val requestModel = globalGson.fromJson(InputStreamReader(request.inputStream, StandardCharsets.UTF_8), Map::class.java)
                        val sql = requestModel["url"].toString()
                        val formatter = Formatter.getFormatter(requestModel["format"]?.toString()
                                ?: baseRequest.queryParameters.getString("format") ?: config.format, config)
                        sql to formatter
                    }
                    else -> {
                        val sql = CharStreams.toString(InputStreamReader(request.inputStream, StandardCharsets.UTF_8))
                        val formatter = Formatter.getFormatter(baseRequest.queryParameters.getString("format") ?: config.format, config)
                        sql to formatter
                    }
                }

                val runner = SqlRunner(connection, formatter)
                val writer = OutputStreamWriter(response.outputStream)
                try {
                    runner.run(sql, writer)
                } catch (e: SQLException) {
                    response.status = 400
                    formatter.formatError(e, writer)
                } catch (e: Exception) {
                    response.status = 500
                    formatter.formatError(e, writer)
                }

                writer.flush()
                baseRequest.isHandled = true
            }
        }
        server.start()
        server.join()
    }
}
