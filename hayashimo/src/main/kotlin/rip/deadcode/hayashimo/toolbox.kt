package rip.deadcode.hayashimo

import com.google.gson.GsonBuilder

val globalGson = GsonBuilder()
        .serializeNulls()
        .disableHtmlEscaping()
        .create()

val globalGsonFromatting = GsonBuilder()
        .setPrettyPrinting()
        .serializeNulls()
        .disableHtmlEscaping()
        .create()
