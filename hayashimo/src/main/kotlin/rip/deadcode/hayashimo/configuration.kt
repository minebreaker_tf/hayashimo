package rip.deadcode.hayashimo

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.Options
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

fun readConfig(path: String?, commandLine: CommandLine): Configuration {
    val fileConfig = if (path != null) {
        readConfig(Paths.get(path))
    } else {
        ConfigFactory.empty()
    }

    return Configuration(
            url = commandLine.getOptionValue("url") ?: fileConfig.getString("url"),
            user = commandLine.getOptionValue("user") ?: fileConfig.getOptionalString("user"),
            password = commandLine.getOptionValue("password") ?: fileConfig.getOptionalString("password"),
            output = commandLine.getOptionValue("out") ?: fileConfig.getOptionalString("output"),
            input = commandLine.getOptionValue("in") ?: fileConfig.getOptionalString("input"),
            format = commandLine.getOptionValue("format") ?: fileConfig.getOptionalString("format") ?: "csv",
            prettyPrint = commandLine.hasOption("pretty-print") || fileConfig.getOptionalBoolean("prettyPrint", true),
            header = commandLine.hasOption("header") || fileConfig.getOptionalBoolean("header", false),
            linebreak = LineBreak.fromString(commandLine.getOptionValue("linebreak") ?: fileConfig.getOptionalString("linebreak") ?: "n"),
            server = commandLine.hasOption("server") || fileConfig.getOptionalBoolean("server", false),
            port = commandLine.getOptionValue("port")?.toInt() ?: fileConfig.getOptionalInt("port", 8080)
    )
}

fun Config.getOptionalString(path: String): String? =
        if (this.hasPath(path)) this.getString(path) else null

fun Config.getOptionalBoolean(path: String, default: Boolean) =
        if (this.hasPath(path)) this.getBoolean(path) else default

fun Config.getOptionalInt(path: String, default: Int) =
        if (this.hasPath(path)) this.getInt(path) else default

fun readConfig(path: Path): Config {
    return Files.newBufferedReader(path).use {
        ConfigFactory.parseReader(it)
    }
}

data class Configuration(
        val url: String,
        val user: String?,
        val password: String?,
        val output: String?,
        val input: String?,
        val format: String,
        val prettyPrint: Boolean,
        val header: Boolean,
        val linebreak: LineBreak,
        val server: Boolean,
        val port: Int
)

enum class LineBreak(val character: String) {
    LF("\n"),
    CRLF("\r\n")
    ;

    companion object {
        fun fromString(str: String): LineBreak = when (str) {
            "n" -> LF
            "rn" -> CRLF
            else -> throw IllegalArgumentException("Unknown parameter for the option linebreak: ${str}")
        }
    }
}

val options: Options = Options()
        .addOption("h", "help", false, "Shows help.")
        .addOption("u", "url", true, "JDBC URL.")
        .addOption("a", "user", true, "JDBC User.")
        .addOption("p", "password", true, "JDBC password.")
        .addOption("o", "out", true, "If specified, writes results to the file instead of stdout.")
        .addOption("i", "in", true, "If specified, reads SQL from the file instead of stdin.")
        .addOption("f", "format", true, "Output format. Valid values are: 'csv' | 'json'.")
        .addOption(null, "pretty-print", false, "Pretty prints the output.")
        .addOption(null, "header", true, "If set 'true', adds header to the output. Available when format is 'csv'. Default: true")
        .addOption("c", "config", true, "Specifies a configuration file.")
        .addOption("x", "properties")
        .addOption(null, "linebreak", true, "Specifies line break characters. Valid values are: 'n' | 'rn'")
        .addOption(null, "server", false, "Server mode.")
        .addOption(null, "port", true, "TCP Port for the server mode.")
