package rip.deadcode.hayashimo.formatter

import com.google.common.io.CharStreams
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import java.sql.ResultSet
import java.sql.Types
import java.sql.Types.BIGINT
import java.sql.Types.BINARY
import java.time.format.DateTimeFormatter

object SqlTypeConverter {

    fun convert(resultSet: ResultSet, i: Int): Any {
        val metaData = resultSet.metaData
        val type = metaData.getColumnType(i)

        // TODO hex binary output
        // TODO limit maximum length
        // TODO when result contains special chars

        return when (type) {
            BIGINT -> resultSet.getBigDecimal(i)
            BINARY -> CharStreams.toString(InputStreamReader(resultSet.getBinaryStream(i), StandardCharsets.UTF_8))
            Types.BLOB -> CharStreams.toString(InputStreamReader(resultSet.getBlob(i).binaryStream, StandardCharsets.UTF_8))
            Types.BOOLEAN -> resultSet.getBoolean(i)
            Types.CHAR -> resultSet.getString(i)
            Types.CLOB -> CharStreams.toString(resultSet.getCharacterStream(i))
            Types.DATE -> resultSet.getDate(i).toLocalDate().format(DateTimeFormatter.ISO_DATE_TIME)
            Types.DECIMAL -> resultSet.getBigDecimal(i)
            Types.DOUBLE -> resultSet.getDouble(i)
            Types.FLOAT -> resultSet.getFloat(i)
            Types.INTEGER -> resultSet.getInt(i)
            Types.LONGNVARCHAR -> resultSet.getString(i)
            Types.NCHAR -> resultSet.getString(i)
            Types.NCLOB -> CharStreams.toString(resultSet.getCharacterStream(i))
            Types.NULL -> "null"
            Types.NUMERIC -> resultSet.getBigDecimal(i)
            Types.SMALLINT -> resultSet.getInt(i)
            Types.TIME -> resultSet.getTime(i).toLocalTime().format(DateTimeFormatter.ISO_DATE_TIME)
            Types.TIMESTAMP -> resultSet.getTimestamp(i).toLocalDateTime().format(DateTimeFormatter.ISO_DATE_TIME)
            Types.TIMESTAMP_WITH_TIMEZONE -> resultSet.getTimestamp(i).toLocalDateTime().format(DateTimeFormatter.ISO_DATE_TIME)
            Types.TIME_WITH_TIMEZONE -> resultSet.getTimestamp(i).toLocalDateTime().format(DateTimeFormatter.ISO_DATE_TIME)
            Types.TINYINT -> resultSet.getInt(i)
            Types.VARBINARY -> CharStreams.toString(InputStreamReader(resultSet.getBinaryStream(i), StandardCharsets.UTF_8))
            Types.VARCHAR -> resultSet.getString(i)
            else -> resultSet.getObject(i)
        }
    }
}
