package rip.deadcode.hayashimo.formatter

import com.google.common.base.Throwables
import com.google.common.collect.ImmutableList
import com.google.common.collect.ImmutableMap
import rip.deadcode.hayashimo.Configuration
import rip.deadcode.hayashimo.globalGson
import rip.deadcode.hayashimo.globalGsonFromatting
import java.io.Writer
import java.sql.ResultSet

class JsonFormatter(config: Configuration) : Formatter {

    private val gson = if (config.prettyPrint) globalGsonFromatting else globalGson

    override fun format(resultSet: ResultSet, writer: Writer) {
        val meta = resultSet.metaData
        val columnCount = meta.columnCount

        val listBuilder = ImmutableList.builder<Map<String, Any>>()
        while (resultSet.next()) {
            var i = 1
            val mapBuilder = ImmutableMap.builder<String, Any>()
            while (i <= columnCount) {
                val columnName = meta.getColumnLabel(i)
                val c = SqlTypeConverter.convert(resultSet, i)
                mapBuilder.put(columnName, c)
                i++
            }
            listBuilder.add(mapBuilder.build())
        }

        gson.toJson(listBuilder.build(), writer)
    }

    override fun formatUpdate(updateCount: Int, writer: Writer) {
        writer.write(gson.toJson(mapOf("updateCount" to updateCount)))
    }

    override fun formatError(exception: Exception, writer: Writer) {
        val stacktrace = Throwables.getStackTraceAsString(exception)
        writer.write(gson.toJson(mapOf("exception" to stacktrace)))
    }
}
