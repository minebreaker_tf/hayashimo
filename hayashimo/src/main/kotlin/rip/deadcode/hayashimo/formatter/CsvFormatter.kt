package rip.deadcode.hayashimo.formatter

import com.google.common.base.Throwables
import rip.deadcode.hayashimo.Configuration
import java.io.Writer
import java.sql.ResultSet

class CsvFormatter(private val config: Configuration) : Formatter {

    override fun format(resultSet: ResultSet, writer: Writer) {
        // TODO pretty

        val meta = resultSet.metaData
        val columnCount = meta.columnCount

        if (config.header) {
            var i = 1
            while (i <= columnCount) {
                writer.write(meta.getColumnLabel(i))

                if (i != columnCount) {
                    writer.write(",")
                }
                i++
            }
        }

        while (resultSet.next()) {
            var i = 1
            while (i <= columnCount) {
                val c = SqlTypeConverter.convert(resultSet, i)
                writer.write(c.toString())  // TODO quote

                if (i != columnCount) {
                    writer.write(",")
                }
                i++
            }

            writer.write("\n")
        }
    }

    override fun formatUpdate(updateCount: Int, writer: Writer) {
        if (config.header) {
            writer.write("updateCount\n")
        }
        writer.write(updateCount.toString())
    }

    override fun formatError(exception: Exception, writer: Writer) {
        if (config.header) {
            writer.write("exception\n")
        }
        val stacktrace = Throwables.getStackTraceAsString(exception)
        writer.write(stacktrace)
    }
}
