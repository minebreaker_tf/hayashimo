package rip.deadcode.hayashimo.formatter

import rip.deadcode.hayashimo.Configuration
import java.io.Writer
import java.sql.ResultSet

interface Formatter {

    fun format(resultSet: ResultSet, writer: Writer)

    fun formatUpdate(updateCount: Int, writer: Writer)

    fun formatError(exception: Exception, writer: Writer)

    companion object {
        fun getFormatter(formatterName: String, config: Configuration): Formatter {
            return when (formatterName) {
                "csv" -> CsvFormatter(config)
                "json" -> JsonFormatter(config)
                else -> throw RuntimeException()
            }
        }
    }
}
