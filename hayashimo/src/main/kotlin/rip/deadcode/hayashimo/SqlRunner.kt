package rip.deadcode.hayashimo

import rip.deadcode.hayashimo.formatter.Formatter
import java.io.Writer
import java.sql.Connection

class SqlRunner(
        private val connection: Connection,
        private val formatter: Formatter) {

    fun run(sql: String, writer: Writer) {

        val stmt = connection.prepareStatement(sql)
        stmt.use {
            val hasResultSet = stmt.execute()

            if (hasResultSet) {
                val resultSet = stmt.resultSet
                resultSet.use {
                    formatter.format(resultSet, writer)
                }
            } else {
                val updateCount = stmt.updateCount
                formatter.formatUpdate(updateCount, writer)
            }
        }
    }
}
