# Hayashimo - JDBC runner


## Get started

Requires Java 8+

`java -cp "hayashimo.jar;h2.jar" --url "jdbc://h2:~/test" "select 'hello, world' from dual;"`

`hello, world`


## Arguments

| Argument | Values |
|:---|:---|
| `-o <<file>>` `--out <<file>>` | File to output. If omitted, output to stdout. |
| `-p <<name>>` `--preset <<name>>` | Specify preset name. |
| `-i` `--interactive` | Interactive mode |
| `-f <<format>>` `--format <<format>>` | Output format |
| `-s` `--server` | Server mode |
| `--port` | Port of the server |
| `--parameter <<json>>` | Pass parameters as a JSON. |


## Configuration

```json
{
  "format": {
    "default": "json",
    "linebreak": "LF",
    "csv": { "header": true, "separator": ",", "escape": "\\", "quote": "always", "quoteBy": "\"" },
    "json": { "returnRawSql": true, "prettyPrint": true }
  },
  "server": {
    "port": 8080
  }
}
```


## JDBC Preset

```json
[
  {
    "name": "name_of_preset",
    "url": "default JDBC URL",
    "user": "default user",
    "password": "default password",
    "properties": {
      "jdbc.property.name": "it's value"
    }
  }
]
```


## Stateful execution

### Save SQLs

```
preset "name of preset"
save saved_sql "select * from table where id = ?";
recall saved_sql(123) 
```


## Server mode

1. Run the server with arg `--server`
2. POST json `{ "sql": "<<SQL>>" }` to `http://<<host>>:<<port>`
3. Result is returned.

| HTTP Status | |
|:---|:---|
| 200 | Successfully executed. |
| 400 | SQL execution was failed. |
| 404 | Even if there is no result, 404 will NOT be served. |
| 500 | Something went wrong. |


## Plugins

TODO


## VS Code extension

VS Code extension will be available.
